The Derivicon (DERIVative favICON) module adds a Context module reaction
allowing for Image style powered favicon derivatives to be applied.

Derivicon was built to help with identifying different server environments at a
glance of the browser tab.

Note: As the GD library doesn't support .ico files, it is recommended that you
      either use PNG files (renamed as .ico) as favicons, or install
      ImageMagick.



Required modules
--------------------------------------------------------------------------------

* Context            - http://drupal.org/project/context
* Drupal core Image module.



Recommended modules
--------------------------------------------------------------------------------

* ImageCache Actions - http://drupal.org/project/imagecache_actions
* ImageMagick        - http://drupal.org/project/imagemagick
* Context URL        - http://drupal.org/sandbox/gianfrasoft/1387780

Note: Context URL is a Sandbox that as of the release of this module was not yet
      ready for release and required a patch
      (http://drupal.org/node/1947330#comment-7196588) to make it work
      correctly.
